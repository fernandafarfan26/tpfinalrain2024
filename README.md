# TwitterProfileAnalyzerApp

## Descripción

`TwitterProfileAnalyzerApp` es una aplicación de escritorio desarrollada en Python que permite analizar el perfil de un usuario de Twitter. La aplicación recupera tweets de un usuario especificado, realiza análisis de sentimiento en los tweets, extrae temas predominantes y visualiza la frecuencia de las palabras más comunes. La interfaz gráfica de usuario (GUI) está construida utilizando `tkinter`.

## Requisitos

### Bibliotecas y Módulos de Python

- `tkinter`: Para la creación de la interfaz gráfica de usuario.
- `nltk`: Biblioteca de procesamiento de lenguaje natural (Natural Language Toolkit).
- `re`: Módulo para manejar expresiones regulares.
- `ntscraper`: Módulo personalizado para scraping de Twitter.
- `matplotlib`: Para la creación de gráficos y visualizaciones.
- `sklearn`: Biblioteca de aprendizaje automático.
- `langdetect`: Para detectar automáticamente el idioma de los textos.
- `threading`: Para ejecutar procesos en segundo plano.
- `os`: Para operaciones del sistema operativo.
- `logging`: Para registrar eventos y errores.

### Instalación de Dependencias

1. **Instalar las dependencias requeridas**:

    ```bash
    pip install nltk matplotlib scikit-learn langdetect
    ```

2. **Descargar recursos necesarios de NLTK**:

    ```python
    import nltk
    nltk.download('vader_lexicon')
    nltk.download('stopwords')
    ```

## Instrucciones de Uso

### Ejecutar la Aplicación

1. **Descargar el proyecto** y navegar a la carpeta del proyecto.
2. **Ejecutar el script principal**:

    ```bash
    python Xanalisis.py
    ```

### Interacción con la GUI

1. **Ingresar el nombre de usuario de Twitter** en el campo correspondiente.
2. **Ingresar la cantidad de tweets** que se desea recuperar.
3. **Hacer clic en el botón "Buscar Tweets"** para iniciar el análisis.

### Resultados del Análisis

- Los tweets recuperados y sus análisis de sentimiento se mostrarán en el área de texto de la interfaz.
- Se abrirá una ventana emergente con los temas extraídos de los tweets.
- Se generará una gráfica con la frecuencia de las palabras más comunes en los tweets.

## Explicación del Código

### Estructura Principal

#### Clase `TwitterProfileAnalyzerApp`

- **`__init__(self, root)`**: Inicializa la aplicación y configura la interfaz de usuario.
- **`setup_ui(self)`**: Configura el título y el color de fondo de la ventana principal.
- **`create_widgets(self)`**: Crea y configura los widgets en la interfaz de usuario.

#### Funciones de la Clase

- **`get_tweets(self, user, num_tweets=50)`**: Obtiene tweets de un usuario de Twitter usando `Nitter`.
- **`preprocess_text(self, text)`**: Limpia el texto de los tweets eliminando URLs, menciones, hashtags y caracteres no alfanuméricos.
- **`analyze_sentiment(self, tweets)`**: Analiza el sentimiento de cada tweet usando `VADER Sentiment Analyzer`.
- **`extract_topics(self, tweets, num_topics=5)`**: Extrae temas de los tweets usando `Latent Dirichlet Allocation (LDA)`.
- **`plot_word_frequency(self, tweets)`**: Grafica la frecuencia de las palabras más comunes en los tweets.
- **`show_topics_popup(self, topics)`**: Muestra una ventana emergente con los temas extraídos de los tweets.
- **`analyze_profile(self)`**: Analiza el perfil de Twitter al hacer clic en el botón "Buscar Tweets".
- **`analyze_tweets(self, user, number)`**: Realiza el análisis completo de los tweets del usuario.

## Personalización

### Modificar la Apariencia de la Interfaz

- **Colores y diseño**: Los colores de fondo, texto y botones se pueden personalizar cambiando los valores de `bg_color`, `fg_color`, `entry_bg_color`, `button_bg_color` y `button_fg_color` en la función `create_widgets`.

### Ajustar el Análisis de Sentimientos y Temas

- **Número de temas**: El número de temas extraídos se puede ajustar cambiando el valor del parámetro `num_topics` en la función `extract_topics`.

### Manejo de Idiomas

- **Detección automática del idioma**: La aplicación detecta automáticamente el idioma predominante de los tweets y ajusta las stopwords en consecuencia.


## Contacto

Para cualquier consulta o sugerencia, por favor contacta a [fernandafarfan26@gmail.com].

---

