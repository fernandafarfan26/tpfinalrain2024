import tkinter as tk
from tkinter import messagebox, Text, Entry
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import nltk
import re
from ntscraper import Nitter
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
import langdetect
from nltk.corpus import stopwords
import threading
import os
import logging

class TwitterProfileAnalyzerApp:
    def __init__(self, root):
        self.root = root
        self.setup_ui()

    def setup_ui(self):
        # Configurar el título y el color de fondo de la ventana principal
        self.root.title("análisis")
        self.root.configure(bg="#2E2E2E")

        # Crear widgets en la interfaz de usuario
        self.create_widgets()

    def create_widgets(self):
        # Definir colores utilizados en la interfaz
        bg_color = "#2E2E2E"
        fg_color = "#FFFFFF"
        entry_bg_color = "#3E3E3E"
        button_bg_color = "#555555"
        button_fg_color = "#FFFFFF"

        # Icono de la ventana
        script_dir = os.path.dirname(os.path.abspath(__file__))
        icon_path = os.path.join(script_dir, 'x.ico')
        try:
            self.root.iconbitmap(icon_path)
        except Exception as e:
            logging.error(f"No se pudo cargar el icono: {e}")

        # Elementos de la GUI
        # Etiqueta y entrada para el nombre de usuario de Twitter
        self.user_label = tk.Label(self.root, text="Ingrese el nombre de usuario de Twitter:", bg=bg_color, fg=fg_color)
        self.user_label.pack(pady=5)
        self.user_entry = Entry(self.root, width=50, bg=entry_bg_color, fg=fg_color, insertbackground=fg_color)
        self.user_entry.pack(pady=5)

        # Etiqueta y entrada para la cantidad de tweets a recuperar
        self.number_label = tk.Label(self.root, text="Ingrese la cantidad de tweets a recuperar:", bg=bg_color, fg=fg_color)
        self.number_label.pack(pady=5)
        self.number_entry = Entry(self.root, width=50, bg=entry_bg_color, fg=fg_color, insertbackground=fg_color)
        self.number_entry.pack(pady=5)

        # Botón para iniciar la búsqueda de tweets
        self.search_button = tk.Button(self.root, text="Buscar Tweets", command=self.analyze_profile, bg=button_bg_color, fg=button_fg_color)
        self.search_button.pack(pady=10)

        # Área de texto para mostrar los resultados
        self.result_text = Text(self.root, height=20, width=80, bg=entry_bg_color, fg=fg_color, insertbackground=fg_color)
        self.result_text.pack(pady=10)

    def get_tweets(self, user, num_tweets=50):
        # Función para obtener tweets de un usuario de Twitter usando Nitter
        nitter = Nitter()
        tweets = []
        fetched_tweets = nitter.get_tweets(user, mode='user', number=num_tweets*2)
        if fetched_tweets and 'tweets' in fetched_tweets:
            # Filtrar solo tweets originales (no retweets)
            tweets = [tweet['text'] for tweet in fetched_tweets['tweets'] if not tweet.get('is-retweet')]
        return tweets[:num_tweets]

    def preprocess_text(self, text):
        # Función para preprocesar texto de tweets: eliminar URLs, menciones, hashtags y caracteres no alfanuméricos
        text = re.sub(r'http\S+', '', text)
        text = re.sub(r'@\w+', '', text)
        text = re.sub(r'#\w+', '', text)
        text = re.sub(r'\W', ' ', text)
        text = re.sub(r'\s+', ' ', text).strip()
        return text

    def analyze_sentiment(self, tweets):
        # Función para analizar el sentimiento de cada tweet usando VADER Sentiment Analyzer
        sid = SentimentIntensityAnalyzer()
        sentiments = []
        for tweet in tweets:
            score = sid.polarity_scores(tweet)
            sentiments.append(score)
        return sentiments

    def extract_topics(self, tweets, num_topics=5):
        # Función para extraer temas de los tweets usando Latent Dirichlet Allocation (LDA)
        predominant_language = langdetect.detect(" ".join(tweets))
        stop_words = stopwords.words('spanish') if predominant_language == 'es' else stopwords.words('english')
        
        vectorizer = CountVectorizer(max_df=0.95, min_df=1, stop_words=stop_words)
        tf = vectorizer.fit_transform(tweets)
        
        lda_model = LatentDirichletAllocation(n_components=num_topics, random_state=42)
        lda_model.fit(tf)
        
        feature_names = vectorizer.get_feature_names_out()
        topics = {f"Tópico {idx+1}": [feature_names[i] for i in topic.argsort()[:-10 - 1:-1]] for idx, topic in enumerate(lda_model.components_)}
        
        return topics

    def plot_word_frequency(self, tweets):
        # Función para graficar la frecuencia de las palabras más comunes en los tweets
        def show_plot():
            all_text = ' '.join(tweets)
            words = all_text.split()

            predominant_language = langdetect.detect(" ".join(tweets))
            stop_words = stopwords.words('spanish') if predominant_language == 'es' else stopwords.words('english')
            
            filtered_words = [word for word in words if word.lower() not in stop_words]

            word_freq = nltk.FreqDist(filtered_words)
            most_common_words = word_freq.most_common(10)
            words, freqs = zip(*most_common_words)

            plt.figure(figsize=(10, 6))
            plt.bar(words, freqs)
            plt.title('Frecuencia de Palabras más Comunes')
            plt.xlabel('Palabra')
            plt.ylabel('Frecuencia')
            plt.xticks(rotation=45)
            plt.tight_layout()
            plt.show()

        # Mostrar la gráfica en el hilo principal usando self.root.after_idle()
        self.root.after_idle(show_plot)

    def show_topics_popup(self, topics):
        # Función para mostrar un popup con los temas extraídos de los tweets
        popup = tk.Toplevel(self.root)
        popup.title("Análisis de Temas")
        popup.geometry("600x400")
        popup.configure(bg="#2E2E2E")

        topics_text = Text(popup, height=20, width=50, bg="#3E3E3E", fg="#FFFFFF", insertbackground="#FFFFFF")
        topics_text.pack(pady=10)

        topics_text.insert(tk.END, "Temas Extraídos:\n\n")
        for topic, words in topics.items():
            topics_text.insert(tk.END, f"{topic}: {', '.join(words)}\n")

    def analyze_profile(self):
        # Función para analizar el perfil de Twitter al hacer clic en el botón 'Buscar Tweets'
        user = self.user_entry.get().strip()
        number = self.number_entry.get().strip()

        if not user:
            messagebox.showerror("Error de entrada", "El nombre de usuario no puede estar vacío.")
            return

        try:
            number = int(number)
        except ValueError:
            messagebox.showerror("Error de entrada", "La cantidad de tweets debe ser un número válido.")
            return

        self.result_text.delete(1.0, tk.END)
        self.result_text.insert(tk.END, f"Buscando tweets de @{user}...\n")

        threading.Thread(target=self.analyze_tweets, args=(user, number)).start()

    def analyze_tweets(self, user, number):
        # Función para realizar el análisis completo de los tweets del usuario
        try:
            logging.debug("Iniciando la recuperación de tweets")
            tweets = self.get_tweets(user, number)

            if not tweets:
                self.result_text.insert(tk.END, "No se encontraron tweets.")
                return

            clean_tweets = [self.preprocess_text(tweet) for tweet in tweets]

            sentiment_results = self.analyze_sentiment(clean_tweets)

            topics = self.extract_topics(clean_tweets)

            for tweet, sentiment in zip(clean_tweets, sentiment_results):
                self.result_text.insert(tk.END, f"Tweet: {tweet}\n")
                self.result_text.insert(tk.END, f"Sentimiento: {sentiment}\n")
                self.result_text.insert(tk.END, "-"*80 + "\n")

            messagebox.showinfo("Éxito", "Análisis completado.")

            self.show_topics_popup(topics)

            self.plot_word_frequency(clean_tweets)
        except Exception as e:
            logging.error(f"Error al analizar los tweets: {e}")
            messagebox.showerror("Error", f"Error al analizar los tweets: {e}")

if __name__ == "__main__":
    # Descargar recursos necesarios para NLTK
    nltk.download('vader_lexicon')
    nltk.download('stopwords')

    # Inicializar la ventana de la aplicación
    root = tk.Tk()
    app = TwitterProfileAnalyzerApp(root)
    root.mainloop()
